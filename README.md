BioLite is developed by the [Dunn Lab](http://dunnlab.org) at Brown University.


Quick Install
=============

On **Mac OS X 10.8**, install the Xcode
[Command Line Tools](https://developer.apple.com/downloads) (note: requires an
Apple ID) and [Homebrew](http://mxcl.github.com/homebrew/). Then run 
`brew doctor` to make sure Homebrew is configured properly. If there are any 
warning or errors, address the problems and rerun `brew doctor`. Repeat until 
you get no more warnings or errors. 

Then run:

    sudo easy_install pip
    pip install --user biopython dendropy docutils lxml networkx wget
    brew tap mhowison/science
    export MAXKMERLENGTH=127
    brew install biolite

On **Ubuntu 12.04, 13.04**:

    wget https://bitbucket.org/caseywdunn/biolite/raw/master/install_biolite_ubuntu.sh
    sudo sh install_biolite_ubuntu.sh

See [INSTALL](https://bitbucket.org/caseywdunn/biolite/src/master/INSTALL.md)
for more detailed instructions, system requirements, and for how to configure
BioLite after installing it.


Background
==========

BioLite is a bioinformatics framework written in Python/C++ that automates the
collection and reporting of diagnostics, tracks provenance, and provides
lightweight tools for building out customized analysis pipelines.

BioLite provides generalized components aimed at developers of NGS analyses
workflows. These include:

* a 'catalog' database for pairing metadata with and organizing NGS data files
* a global 'diagnostics' database that establishes provenance for analyses and
  archives analysis results that can be accessed across multiple stages of a
  workflow, or from different workflows
* a modular 'pipeline' framework with Python wrappers and workflows for commonly
  used NGS tools, and logging, profiling, and checkpointing functionality
* a C++ library 'seqio' for efficient I/O of large NGS data files


Reporting Problems
==================

Please use the [issue tracker](https://bitbucket.org/caseywdunn/biolite/issues)
at Bitbucket to report and problems you have.

We have tested BioLite on OS X 10.8, Ubuntu 12.04, and CentOS 6.3, and will
provide limited, best-effort support on these platforms.


Updating and Uninstalling 
=========================
 
BioLite is under active development. This means that new updates are not always
compatiable with analyses that have already been run. We make every effort to
avoid changes to the catalog database structure, so there is usually not a need
to re-catalog data when you install a new version. However, you may need to
rerun already completed analyses if you want to generate new reports or use
existing data with new versions of pipelines.

To uninstall after installing with Homebrew on OS X, use:

    brew remove biolite

To uninstall after installing from the source tarball, run the command:

    make uninstall

from the source directory where you built and installed BioLite.


Citing
======

BioLite is still under development, and is an experimental tool that should be
used with care.  Please cite:

Howison, M., Sinnott-Armstrong, N. A., & Dunn, C. W.  BioLite, a lightweight
bioinformatics framework with automated tracking of diagnostics and provenance.
In Proceedings of the 4th USENIX Workshop on the Theory and Practice of
Provenance (TaPP '12), Boston, MA, USA, Jun 2012.
[https://www.usenix.org/system/files/conference/tapp12/tapp12-final5.pdf]

BioLite makes use of many other programs that do much of the heavy lifting of
the analyses. Please be sure to credit these essential components as well.
Check the biolite.cfg file for web links to these programs, where you can find
more information on how to cite them.


Funding
=======

This software has been developed with support from the following US National
Science Foundation grants:

PSCIC Full Proposal: The iPlant Collaborative: A Cyberinfrastructure-Centered
Community for a New Plant Biology (Award Number 0735191)

Collaborative Research: Resolving old questions in Mollusc phylogenetics with
new EST data and developing general phylogenomic tools (Award Number 0844596)

Infrastructure to Advance Life Sciences in the Ocean State (Award Number
1004057)

The Brown University [Center for Computation and Visualization](http://www.brown.edu/Departments/CCV/) has been instrumental to the development of BioLite.


License
=======

Copyright (c) 2012-2013 Brown University. All rights reserved.

BioLite is distributed under the GNU General Public License version
3. For more information, see LICENSE or visit:
[http://www.gnu.org/licenses/gpl.html](http://www.gnu.org/licenses/gpl.html)

BioLite includes source code from the following projects:

* gzstream C++ interface v1.5, which is distributed under the GNU Lesser
  General Public License in `LICENSE.gzstream`
* Bootstrap v2.3.1 CSS style, which is distributed under the Apache License
  v2.0 in `share/bootstrap.min.css`
* jsphylosvg v1.55, which is distributed under the GPL in `LICENSE.jsphylosvg`,
  and which includes Raphael 1.4.3, which is distributed under the MIT license
* D3js v3.1.9, which is distributed under the BSD license in `LICENSE.d3js`

