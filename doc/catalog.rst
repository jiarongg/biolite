Cataloging data
===============

The easiest way to interact with the BioLite catalog is using the `catalog`
script packaged wit BioLite:

::

  $ catalog -h
  usage: catalog [-h] {insert,all,search,sizes} ...
  
  Command-line tool for interacting with the agalma catalog.
  
  agalma maintains a 'catalog' stored in an SQLite database of metadata
  associated with your raw Illumina data, including:
  
  - A unique ID that you make up to reference this data set.
  - Paths to the FASTQ files containing the raw forward and reverse reads.
  - The species name and NCBI ID.
  - The sequencing center where the data was collected.
  
  optional arguments:
    -h, --help            show this help message and exit
  
  commands:
    {insert,all,search,sizes}
      insert              Add a new record to the catalog, or overwrite the
                          existing record with the same id.
      all                 List all catalog entries.
      search              Search all fields (except 'paths') for entries
                          matching the provided pattern, which can include * as
                          a wildcard.
      sizes               List all paths in the catalog, ordered by size on
                          disk.

The documentation below describes the `catalog` module, for manually
interacting with the catalog from within a Python script.

:mod:`catalog` Module
---------------------

.. automodule:: biolite.catalog
    :members:
    :undoc-members:
    :show-inheritance:

