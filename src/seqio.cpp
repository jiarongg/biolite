/* BioLite - Tools for processing gene sequence data and automating workflows
 * Copyright (c) 2012-2013 Brown University. All rights reserved.
 * 
 * This file is part of BioLite.
 * 
 * BioLite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * BioLite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with BioLite.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <algorithm>
#include "seqio.hpp"

#define PROGNAME "seqio"
#include "util.h"

#define FASTA_DELIM '>'
#define FASTQ_DELIM '@'
#define IDLINE_DELIM " \t/"

using namespace std;

/* static functions */

bool SeqIO::match_file_extension(const char* filename, const char* ext)
{
	const char* start = strrchr(filename, '.');
	if (start == NULL) return false;
	if (strcmp(start, ext) == 0) return true;
	else return false;
}

istream* SeqIO::openInput(const char* filename)
{
	istream* input = NULL;
	bool is_open = false;

	if (match_file_extension(filename, ".gz")) {
		NOTIFY("detected GZIP compression for '" << filename << "'")
		igzstream* f = new igzstream(filename);
		is_open = f->rdbuf()->is_open();
		input = f;
	} else {
		ifstream* f = new ifstream(filename);
		is_open = f->is_open();
		input = f;
	}

	if (!is_open)
		PERROR("could not open file '" << filename << "'")

	return input;
}

ostream* SeqIO::openOutput(const char* filename)
{
	ostream* output = NULL;
	bool is_open = false;

	if (match_file_extension(filename, ".gz")) {
		NOTIFY("detected GZIP compression for '" << filename << "'")
		ogzstream* f = new ogzstream(filename);
		is_open = f->rdbuf()->is_open();
		output = f;
	} else {
		ofstream* f = new ofstream(filename);
		is_open = f->is_open();
		output = f;
	}

	if (!is_open)
		PERROR("could not open file '" << filename << "'")

	return output;
}

/* public functions */

SeqIO::SeqIO(const char* filename)
{
	this->filename = filename;
	output = &cout;
	qual_input = NULL;
	nline = 0;

	if (strcmp(filename, "-") == 0) {
		input = &cin;
	} else {
		input = openInput(filename);
	}

	delim = input->peek();
	if (delim == FASTA_DELIM) {
		NOTIFY("detected FASTA format for '" << filename << "'")
	} else if (delim == FASTQ_DELIM) {
		NOTIFY("detected FASTQ format for '" << filename << "'")
	} else {
		ERROR("'" << filename << "' has neither FASTA nor FASTQ format!")
	}
}

SeqIO::~SeqIO()
{
	if (output != &cout) delete output;
	if (input != &cin) delete input;
	if (qual_input != NULL && qual_input != &cin) delete qual_input;
}

bool SeqIO::isFastq()
{
	return (delim == FASTQ_DELIM);
}

void SeqIO::setOutput(const char* filename)
{
	output = openOutput(filename);
}

void SeqIO::setQualityInput(const char* filename, int qual_ascii)
{
	if (delim != FASTA_DELIM)
		ERROR("can only attach a QUAL file to a FASTA file!")
	qual_input = openInput(filename);
	this->qual_ascii = qual_ascii;
}

bool SeqIO::nextRecord()
{
	if (!input->good())
		ERROR("failure reading from file '" << filename << "'!")

	if (!getline(*input, idline)) return false;
	if (!getline(*input, sequence)) return false;

	if (delim == FASTA_DELIM) {
		nline++;
		validateFasta();
	} else {
		if (!getline(*input, plusline)) return false;
		if (!getline(*input, quality)) return false;
		nline++;
		validateFastq();
	}

	return true;
}

bool SeqIO::nextRecordQual()
{
	if (delim != FASTA_DELIM)
		ERROR("QUAL records are only supported for FASTA files!")
	if (!input->good())
		ERROR("failure reading from file '" << filename << "'!")

	/* Read FASTA record. */
	if (!getline(*input, idline)) return false;
	if (!getline(*input, sequence)) return false;
	validateFasta();

	/* Read QUAL record. */
	string qual_idline;
	if (!getline(*qual_input, qual_idline)) return false;
	if (qual_idline.compare(idline) != 0)
		ERROR("FASTA id '" << idline << "' does not match QUAL id '" << qual_idline << "'!")
	if (!getline(*qual_input, quality)) return false;

	if (!qual_ascii) {
		/* Convert the numerical quality scores to ASCII scores. */
		string quality_copy(quality);
		istringstream quality_stream(quality_copy, istringstream::in);
		quality.clear();
		while (quality_stream.good()) {
			int i;
			quality_stream >> i;
			quality.push_back((char)(i + qual_offset));
		}
	}

	nline++;

	return true;
}

unsigned SeqIO::countRecords()
{
	return nline;
}

bool SeqIO::validateFasta()
{
	if (idline.at(0) != FASTA_DELIM)
		ERROR("id line for '" << filename << "':" << nline << " does not start with '" << FASTA_DELIM << "'!")
	return true;
}

bool SeqIO::validateFastq()
{
	if (idline.at(0) != FASTQ_DELIM)
		ERROR("id line for '" << filename << "':" << nline << " does not start with '" << FASTQ_DELIM << "'!")
	if (plusline.at(0) != '+')
		ERROR("sequence/quality separator is not '+' for '" << filename << "':" << nline << "!")
	return true;
}

void SeqIO::reformatID(const char* suffix, size_t n)
{
	size_t loc = idline.find_first_of(IDLINE_DELIM);
	idline.replace(loc, n, suffix);
	idline.resize(loc + n);
}

void SeqIO::setQualityOffset(int offset) { qual_offset = offset; }

int SeqIO::getQualityOffset() { return qual_offset; }

double SeqIO::getMeanQuality()
{
	int qsum = 0;
	int i;
	int qlength = quality.length();
	for (i = 0; i < qlength; i++)
	{
		qsum += (quality[i]);
	}
	
	return (qsum / (double) qlength) - qual_offset;
}

string SeqIO::getRecord()
{
	string record(idline);
	record.push_back('\n');
	record.append(sequence);
	record.push_back('\n');
	if (delim == FASTQ_DELIM) {
		record.append(plusline);
		record.push_back('\n');
		record.append(quality);
		record.push_back('\n');
	}
	return record;
}

string SeqIO::getID()
{
	size_t end = idline.find_first_of(IDLINE_DELIM);
	return idline.substr(1, end - 1);
}

string& SeqIO::getIDLine() { return idline; }
string& SeqIO::getSequence() { return sequence; }
string& SeqIO::getQuality() { return quality; }

void SeqIO::printRecord() { printRecord(*output); }
void SeqIO::printRecord(ostream& output)
{
	output << idline << endl << sequence << endl;
	if (delim == FASTQ_DELIM)
		output << plusline << endl << quality << endl;
}

void SeqIO::printRecordFastq() { printRecordFastq(*output); }
void SeqIO::printRecordFastq(ostream& output)
{
	idline.at(0) = FASTQ_DELIM;
	output << idline << endl << sequence << endl << '+' << endl << quality <<endl;
}

void SeqIO::printRecordFasta() { printRecordFasta(*output); }
void SeqIO::printRecordFasta(ostream& output)
{
	idline.at(0) = FASTA_DELIM;
	output << idline << endl << sequence << endl;
}

void SeqIO::printRecordQual(int ascii) { printRecordQual(*output, ascii); }
void SeqIO::printRecordQual(ostream& output, int ascii)
{
	idline.at(0) = FASTA_DELIM;
	output << idline << endl;
	if (ascii) {
		output << quality << endl;
	} else {
		string::iterator it;
		for (it = quality.begin(); it != quality.end(); it++) {
			output << (int)(*it) - qual_offset << " ";
		}
		output << endl;
	}
}

