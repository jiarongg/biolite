/* BioLite - Tools for processing gene sequence data and automating workflows
 * Copyright (c) 2012-2013 Brown University. All rights reserved.
 * 
 * This file is part of BioLite.
 * 
 * BioLite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * BioLite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with BioLite.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <vector>
#include "seqio.hpp"

#define PROGNAME "fastq2fasta"
#include "util.h"

using namespace std;

void print_usage()
{
	cout << "\n"
"usage: fastq2fasta -i FASTQ [...] [-o FASTA ...] [-q QUAL ...] [-a]\n"
"                   [-t OFFSET] [-s SUFFIX]\n"
"\n"
"Converts each FASTQ input file to a FASTA file and quality score file\n"
"with the names <basename>.fasta and <basename>.fasta.qual, where <basename>\n"
"is the name of INPUT up to the last period (or with the names FASTA and QUAL\n"
"if specified).\n"
"\n"
"FASTA and QUAL are *appended* to (not truncated).\n"
"\n"
"  -i  specify multiple FASTQ input files\n"
"  -o  specify multiple (corresponding) FASTA output files\n"
"  -q  specify multiple (corresponding) QUAL output files\n"
"  -a  print ASCII quality scores (default: numerical scores)\n"
"  -t  use OFFSET for converting ASCII quality scores (default: 33)\n"
"  -s  reformat the ID line by replacing everything after a space, tab or / with\n"
"      with the specified SUFFIX\n"
"\n";
}

void fastq_to_fasta(
		const char* fastq_filename,
		const char* fasta_filename,
		const char* qual_filename,
		int offset,
		int qual_ascii,
		const char* suffix
		)
{
	SeqIO fastq(fastq_filename);
	fastq.setQualityOffset(offset);

	ofstream fasta_file(fasta_filename, ofstream::app);
	if (!fasta_file) PERROR("could not open file '" << fasta_filename << "'")
	ofstream qual_file(qual_filename, ofstream::app);
	if (!qual_file) PERROR("could not open file '" << qual_filename << "'")

	size_t nsuffix = 0;
	if (suffix != NULL) nsuffix = strlen(suffix);

	while (fastq.nextRecord()) {
		if (nsuffix) fastq.reformatID(suffix, nsuffix);
		fastq.printRecordFasta(fasta_file);
		fastq.printRecordQual(qual_file, qual_ascii);
	}

	fasta_file.close();
	qual_file.close();

	NOTIFY(fastq.countRecords() << " sequences in " << fastq_filename)
}

int main(int argc, char** argv)
{
	vector<const char*> fastq_names;
	vector<const char*> fasta_names;
	vector<const char*> qual_names;
	int offset = 33;
	int qual_ascii = 0;
	const char* suffix = NULL;

	int c;
	while ((c = getopt(argc, argv, "vhi:o:q:at:s:")) != -1)
		switch (c) {
			case 'i':
				fastq_names.push_back(optarg);
				break;
			case 'o':
				fasta_names.push_back(optarg);
				break;
			case 'q':
				qual_names.push_back(optarg);
				break;
			case 't':
				offset = atoi(optarg);
				break;
			case 'a':
				qual_ascii = 1;
				break;
			case 's':
				suffix = optarg;
				break;
			case 'v':
				PRINT_VERSION
			case 'h':
			default:
				print_usage();
				exit(EXIT_SUCCESS);
		}

	if (fasta_names.size()) {
		if (fastq_names.size() != fasta_names.size())
			ARG_ERROR("mismatch between number of FASTQ and FASTA files")
	}

	if (qual_names.size()) {
		if (fastq_names.size() != qual_names.size())
			ARG_ERROR("mismatch between number of FASTQ and QUAL files")
	}

	if (fastq_names.size() == 0)
		ARG_ERROR("no FASTQ files specified with -i")

	if (suffix != NULL)
		NOTIFY("reformating IDs with suffix '" << suffix << "'")

	for (unsigned i=0; i<fastq_names.size(); i++) {
		const char* fasta_name;
		const char* qual_name;
		/* Get the basename. */
		string fastq_name(fastq_names[i]);
		string base_name(fastq_name, 0, fastq_name.find_last_of('.'));
		string base_name_copy(base_name);
		if (fasta_names.size()) {
			fasta_name = fasta_names[i];
		} else {
			base_name.append(".fasta");
			fasta_name = base_name.c_str();
		}
		if (qual_names.size()) {
			qual_name = qual_names[i];
		} else {
			base_name_copy.append(".fasta.qual");
			qual_name = base_name_copy.c_str();
		}
		NOTIFY("converting '" << fastq_names[i] << "'")
		fastq_to_fasta(
				fastq_names[i], fasta_name, qual_name,
				offset, qual_ascii, suffix);
	}

	return EXIT_SUCCESS;
}

