/* BioLite - Tools for processing gene sequence data and automating workflows
 * Copyright (c) 2012-2013 Brown University. All rights reserved.
 * 
 * This file is part of BioLite.
 * 
 * BioLite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * BioLite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with BioLite.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstring>
#include <cstdlib>
#include <unistd.h>
#include <limits.h>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include "seqio.hpp"

#define PROGNAME "filter_illumina"
#include "util.h"

using namespace std;

void print_usage()
{
	cout << "\n"
"usage: filter_illumina [-i INPUT ...] [-o OUTPUT ...] [-u UNPAIRED-OUTPUT]\n"
"                       [-t OFFSET] [-q QUALITY] [-n NREADS] [-a] [-b] [-s SEP]\n"
"\n"
"Filters out low-quality and adapter-contaminated reads from Illumina data.\n"
"\n"
"If no inputs are specified, input is read from stdin. If no outputs are\n"
"specified, stdout is used.\n"
"\n"
"If multiple input files are specified, these are treated as paired files.\n"
"So if a sequence in one input is filtered, its pair is also filtered from\n"
"the same position in all other input files.\n"
"\n"
"  -i  specify multiple INPUT files\n"
"  -o  specify multiple OUTPUT files. If input data are paired, these files\n"
"      contain only reads that passed for both members of the pair\n"
"  -u  specify single OUTPUT file for reads that passed for one member of a\n" 
"      pair but not the other. These reads passed the filter criteria, but\n" 
"      are excluded from the multiple OUTPUT files specified by -o because \n"
"      their paired read did not pass\n"
"  -t  ascii OFFSET for the quality score (default = 33)\n"
"  -q  filter out reads that have a mean quality less than QUALITY\n"
"  -n  limit output to only the first NREADS that pass\n"
"  -a  remove Illumina adapter sequences\n"
"  -b  remove sequences with suspicious base content\n"
"  -s  reformat paired read ID lines by replacing everything after the first\n"
"      space, tab or / with the specified SEP followed by the input file number\n"
"      (e.g. '/' will use /1 as the ID suffix for the first input file, etc.)\n"
"\n"
"Example usage on paired-end data:\n"
"filter_illumina -i 1.fastq -i 2.fastq -o 1.filtered.fastq -o 2.filtered.fastq\n"
"\n";
}

/* Initialize the adapter sequences. */
vector<string> adapters;

void init_adapters()
{
	// Shared by all old and truseq illumina adapters, should filter reads with adapters unless they have an error
	adapters.push_back("AGATCGGAAGAGC");
	// The following are adjacent to the 3' of the above shared sequence, they are added for redundancy in case there is a mution in the shared sequence
	adapters.push_back("GTTCAGCAGGAATG"); // Seen in read 1 of old chemistry
	adapters.push_back("TCGTGTAGGGAAAG"); // Seen in read 2 of old chemistry
	adapters.push_back("GTCGTGTAGGGAAA"); // TruSeq universal internal sequence
	adapters.push_back("CACACGTCTGAACT"); // TruSeq indexed adapters internal sequence
}

void filter_illumina(
		vector<SeqIO*>& inputs,
		ostream *unpaired_output,
		int quality,
		int remove_adapters,
		int base_content,
		const char* sep,
		long nlimit
		)
{
	/* Keep track of sequences read and kept. */
	long pairs = 0;
	long pairs_kept = 0;
	long unpaired_kept = 0;

	/* Keep track of rejections. */
	long reject_quality = 0;
	long reject_adapters = 0;
	long reject_content = 0;

	float const hicut = 0.6;
	float const locut = 0.05;

	bool good = true;

	/* Separator */
	vector<size_t> nsuffix;
	vector<char*> suffix;
	if (sep != NULL) {
		for (unsigned i=0; i<inputs.size(); i++) {
			char* s = (char*)malloc(strlen(sep) + inputs.size());
			ALLOC_CHECK(s)
			sprintf(s, "%s%d", sep, i+1);
			suffix.push_back(s);
			nsuffix.push_back(strlen(s));
		}
	}


	/* Advance all of the input files. */
	for (unsigned i=0; i<inputs.size(); i++) {
		/* All inputs must have a record in order to proceed. */
		good &= inputs[i]->nextRecord();
	}

	std::vector<bool> keep_seq(inputs.size());

	while (good)
	{
		pairs++;
		bool keep_pair = true;

		for (unsigned i=0; i<inputs.size(); i++)
		{
			keep_seq[i] = true;

			/* Test quality score. */
			if (inputs[i]->getMeanQuality() < quality) {
				reject_quality++;
				keep_seq[i] = false;
				keep_pair = false;
			}

			/* Test for adapater contamination. */
			if (keep_seq[i] && remove_adapters) {
				string seq = inputs[i]->getSequence();
				for (unsigned j=0; j<adapters.size(); j++) {
					if (seq.find(adapters[j]) != string::npos) {
						reject_adapters++;
						keep_seq[i] = false;
						keep_pair = false;
						break;
					}
				}
			}

			/* Test for suspicious base content. */
			if (keep_seq[i] && base_content) {
				string seq = inputs[i]->getSequence();
				float length = (float)seq.size();
				float a = (float)count(seq.begin(), seq.end(), 'A') / length;
				float c = (float)count(seq.begin(), seq.end(), 'C') / length;
				float g = (float)count(seq.begin(), seq.end(), 'G') / length;
				float t = (float)count(seq.begin(), seq.end(), 'T') / length;
				if (	a > hicut || a < locut ||
						c > hicut || c < locut ||
						g > hicut || g < locut ||
						t > hicut || t < locut )
				{
					reject_content++;
					keep_seq[i] = false;
					keep_pair = false;
				}
			}
		} // for

		if (keep_pair) {
			pairs_kept++;
			for (unsigned i=0; i<inputs.size(); i++) {
				/* Reformat separator */
				if (sep != NULL) inputs[i]->reformatID(suffix[i], nsuffix[i]);
				inputs[i]->printRecord();
			}
		} else if (unpaired_output) {
			for (unsigned i=0; i<inputs.size(); i++) {
				if (keep_seq[i]) {
					unpaired_kept++;
					inputs[i]->printRecord(*unpaired_output);
				}
			}
		}

		/* Advance again. */
		for (unsigned i=0; i<inputs.size(); i++)
			good &= inputs[i]->nextRecord();

		/* Must be under the read limit to continue. */
		good &= (pairs_kept < nlimit);
	}

	DIAGNOSTICS("pairs", pairs, "(sequence pairs found)")
	DIAGNOSTICS("pairs_kept", pairs_kept, "(pairs remaining after filtering)")
	DIAGNOSTICS("unpaired_kept", unpaired_kept, "(unpaired sequences kept)")
	DIAGNOSTICS("reject_quality", reject_quality, "(sequences rejected for quality)")
	DIAGNOSTICS("reject_adapters", reject_adapters, "(sequences rejected for adapters)")
	DIAGNOSTICS("reject_content", reject_content, "(sequences rejected for suspicious base content)")
}

int main(int argc, char** argv)
{
	vector<const char*> input_names;
	vector<const char*> output_names;
	const char *unpaired_output_name = 0;
	int offset = 33;
	int quality = 28;
	int remove_adapters = 0;
	int base_content = 0;
	long nlimit = LONG_MAX;
	const char* sep = NULL;

	int c;
	while ((c = getopt(argc, argv, "vhi:o:u:t:n:q:abs:")) != -1)
		switch (c) {
			case 'i':
				input_names.push_back(optarg);
				break;
			case 'o':
				output_names.push_back(optarg);
				break;
			case 'u':
				unpaired_output_name = optarg;
				break;
			case 't':
				offset = atoi(optarg);
				break;
			case 'n':
				nlimit = atol(optarg);
				break;
			case 'q':
				quality = atof(optarg);
				break;
			case 'a':
				remove_adapters = 1;
				break;
			case 'b':
				base_content = 1;
				break;
			case 's':
				sep = optarg;
				break;
			case 'v':
				PRINT_VERSION
			case 'h':
			default:
				print_usage();
				exit(EXIT_SUCCESS);
		}

	/* If the number of inputs and outputs don't match, and the number of
	   outputs is non-zero, there is a problem! (i.e. how do you map one input
	   stream to multiple output files?) */
	if (input_names.size() != output_names.size() && output_names.size() > 0)
		ARG_ERROR("mismatch between number of input and output files")

	if (sep != NULL)
		NOTIFY("reformating IDs with separator '" << sep << "'")

	/* Default to stdin for input. */
	if (input_names.size() == 0) input_names.push_back("-");

	/* Open the input and output files. */
	vector<SeqIO*> inputs;
	for (unsigned i=0; i<input_names.size(); i++) {
		inputs.push_back(new SeqIO(input_names[i]));
		inputs[i]->setQualityOffset(offset);
		if (output_names.size() > 0) {
			inputs[i]->setOutput(output_names[i]);
		}
	}

	ostream* unpaired_output = 0;
	if (unpaired_output_name)
		unpaired_output = SeqIO::openOutput(unpaired_output_name);

	/* Perform the filtering. */
	init_adapters();
	filter_illumina(
			inputs, unpaired_output,
			quality, remove_adapters, base_content, sep, nlimit);

	return EXIT_SUCCESS;
}
