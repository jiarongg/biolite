/* BioLite - Tools for processing gene sequence data and automating workflows
 * Copyright (c) 2012-2013 Brown University. All rights reserved.
 * 
 * This file is part of BioLite.
 * 
 * BioLite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * BioLite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with BioLite.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstring>
#include <cstdlib>
#include <unistd.h>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include "hashmap.hpp"
#include "seqio.hpp"

#define PROGNAME "pair_reads"
#include "util.h"

using namespace std;

void print_usage()
{
	cout << "\n"
"usage: pair_reads [-i FASTQ ...] -o BASENAME [-s SEP]\n"
"\n"
"Builds a hash table of reads keyed on read ID for each FASTQ file, and writes\n"
"all paired reads in consistent order to the files BASENAME.pairs.N.fastq,\n"
"where N is the pair number (1,2) found after the separator SEP in the read's ID\n"
"line. All unpaired reads are written to the files BASENAME.single.N.fastq.\n"
"\n"
"Output reads will have the ID followed by /1 or /2 as the ID line.\n"
"\n"
"If no inputs are specified, input is read from stdin.\n"
"\n"
"  -i  specify multiple input FASTQ files\n"
"  -o  specify the BASENAME for the output files\n"
"  -s  use SEP as the separator in the ID line that precedes the pair number\n"
"      [default ' ', for Casava 1.8 headers]\n"
"\n"
"Example usage:\n"
"pair_reads -i 1.fastq -i 2.fastq -o paired\n"
"\n";
}

struct Pair {
	size_t seqno1;
	size_t seqno2;
};

typedef tr1::unordered_map<string, Pair> PairMap;
PairMap pairs;
vector<string> ids;
vector<string> seqs;
vector<string> quals;

void hash_reads(const char* filename, const char* seps)
{
	SeqIO fastq(filename);
	if (!fastq.isFastq())
		ERROR("input file '" << filename << "' is not FASTQ")
	size_t n = 1;
	while (fastq.nextRecord()) {
		string idline = fastq.getIDLine();
		size_t loc = idline.find_first_of(seps);
		if (idline.size() > (loc+1)) {
			char c = idline[loc+1];
			idline.resize(loc);
			/* Load pair. */
			Pair pair;
			if (pairs.count(idline)) {
				pair = pairs[idline];
			} else {
				ids.push_back(idline);
				pair.seqno1 = 0;
				pair.seqno2 = 0;
			}
			/* Set pair entry. */
			if (c == '1') {
				seqs.push_back(fastq.getSequence());
				quals.push_back(fastq.getQuality());
				pair.seqno1 = seqs.size();
			} else if (c == '2') {
				seqs.push_back(fastq.getSequence());
				quals.push_back(fastq.getQuality());
				pair.seqno2 = seqs.size();
			} else  {
				NOTIFY("bad header for sequence #" << n << " in " << filename)
			}
			/* Store pair. */
			pairs[idline] = pair;
		} else {
			NOTIFY("bad header for sequence #" << n << " in " << filename)
		}
		n++;
	}
}

void pair_reads(const char* basename)
{
	string filename(basename);
	ofstream pair1_output((filename + ".pairs.1.fastq").c_str());
	ofstream pair2_output((filename + ".pairs.2.fastq").c_str());
	ofstream single1_output((filename + ".single.1.fastq").c_str());
	ofstream single2_output((filename + ".single.2.fastq").c_str());

	for (size_t i=0; i<ids.size(); i++) {
		Pair pair = pairs[ids[i]];
		if (pair.seqno1 > 0 && pair.seqno2 > 0) {
			pair1_output << ids[i] << "/1" << endl
				<< seqs[pair.seqno1 - 1] << endl
				<< '+' << endl
				<< quals[pair.seqno1 - 1] << endl;
			pair2_output << ids[i] << "/2" << endl
				<< seqs[pair.seqno2 - 1] << endl
				<< '+' << endl
				<< quals[pair.seqno2 - 1] << endl;
		} else if (pair.seqno1 > 0) {
			single1_output << ids[i] << "/1" << endl
				<< seqs[pair.seqno1 - 1] << endl
				<< '+' << endl
				<< quals[pair.seqno1 - 1] << endl;
		} else if (pair.seqno2 > 0) {
			single2_output << ids[i] << "/2" << endl
				<< seqs[pair.seqno2 - 1] << endl
				<< '+' << endl
				<< quals[pair.seqno2 - 1] << endl;
		}
	}

	pair1_output.close();
	pair2_output.close();
	single1_output.close();
	single2_output.close();
}

int main(int argc, char** argv)
{
	vector<const char*> input_names;
	const char* basename = NULL;
	const char* seps = " ";

	int c;
	while ((c = getopt(argc, argv, "vhi:o:s:")) != -1)
		switch (c) {
			case 'i':
				input_names.push_back(optarg);
				break;
			case 'o':
				basename = optarg;
				break;
			case 's':
				seps = optarg;
				break;
			case 'v':
				PRINT_VERSION
			case 'h':
			default:
				print_usage();
				exit(EXIT_SUCCESS);
		}

	/* If the number of inputs and outputs don't match, and the number of
	   outputs is non-zero, there is a problem! (i.e. how do you map one input
	   stream to multiple output files?) */
	if (basename == NULL)
		ARG_ERROR("please specify a BASENAME with -o")

	if (strlen(seps) < 1)
		ARG_ERROR("separator must have at least one character")

	/* Default to stdin for input. */
	if (input_names.size() == 0) input_names.push_back("-");

	for (unsigned i=0; i<input_names.size(); i++)
		hash_reads(input_names[i], seps);

	pair_reads(basename);

	return EXIT_SUCCESS;
}
