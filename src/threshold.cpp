/* BioLite - Tools for processing gene sequence data and automating workflows
 * Copyright (c) 2012-2013 Brown University. All rights reserved.
 * 
 * This file is part of BioLite.
 * 
 * BioLite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * BioLite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with BioLite.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>
#include <iostream>
#include "seqio.hpp"

#define PROGNAME "threshold"
#include "util.h"

using namespace std;

void print_usage()
{
	cout << "\n"
"usage: "PROGNAME" -n THRESHOLD [-s SHORT_READS] [INPUT ...]\n"
"\n"
"Filters the input FASTA or FASTQ files, printing all reads longer than\n"
"THRESHOLD to stdout, and (optionally) all shorter reads to the SHORT_READS\n"
"file. If no input files are specified, input is read from stdin.\n"
"\n"
"  -n THRESHOLD   non-negative length\n"
"  -s SHORT_READS file to print short reads to\n"
"\n";
}

void threshold_seqs(
		const char* filename,
		unsigned threshold,
		ofstream *short_file
		)
{
	int print_short = short_file->is_open();

	SeqIO fastx(filename);
	while (fastx.nextRecord()) {
		if (fastx.getSequence().size() >= threshold) {
			fastx.printRecord();
		} else if (print_short) {
			fastx.printRecord(*short_file);
		}
	}
	NOTIFY(fastx.countRecords() << " sequences in " << filename)
}

int main(int argc, char** argv)
{
	int threshold = -1;
	const char *short_filename = NULL;
	ofstream short_file;

	int c;
	while ((c = getopt(argc, argv, "vhs:n:")) != -1)
		switch (c) {
			case 's':
				short_filename = optarg;
				break;
			case 'n':
				threshold = atoi(optarg);
				break;
			case 'v':
				PRINT_VERSION
			case 'h':
			default:
				print_usage();
				exit(EXIT_SUCCESS);
		}

	if (threshold < 0)
		ARG_ERROR("you must specify a non-negative threshold with -n")

	if (short_filename) {
		short_file.open(short_filename);
		if (!short_file.good())
			ERROR("can't open file '" << short_filename << "' for writing!")
	}

	if (optind == argc) {
		/* Default to stdin for input. */
		threshold_seqs("-", threshold, &short_file);
	} else {
		for (int i=optind; i<argc; i++) {
			NOTIFY("processing '" << argv[i] << "'")
			threshold_seqs(argv[i], (unsigned)threshold, &short_file);
		}
	}

	return EXIT_SUCCESS;
}

