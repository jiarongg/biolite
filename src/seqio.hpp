/* BioLite - Tools for processing gene sequence data and automating workflows
 * Copyright (c) 2012 Brown University. All rights reserved.
 * 
 * This file is part of BioLite.
 * 
 * BioLite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * BioLite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with BioLite.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BIOLITE_SEQIO_HPP__
#define __BIOLITE_SEQIO_HPP__

#include <iostream>
#include <fstream>
#include "gzstream.h"

class SeqIO {

	public:
		/* static functions */
		static bool match_file_extension(const char* filename, const char* ext);
		static std::istream* openInput(const char* filename);
		static std::ostream* openOutput(const char* filename);
		/* functions */
		SeqIO(const char* filename);
		~SeqIO();
		bool isFastq();
		void setOutput(const char* filename);
		void setQualityInput(const char* filename, int qual_ascii);
		bool nextRecord();
		bool nextRecordQual();
		void reformatID(const char* suffix, size_t n);
		void setQualityOffset(int offset);
		int getQualityOffset();
		double getMeanQuality();
		unsigned countRecords();
		std::string getRecord();
		std::string getID();
		std::string& getIDLine();
		std::string& getSequence();
		std::string& getQuality();
		void printRecord();
		void printRecord(std::ostream& output);
		void printRecordFastq();
		void printRecordFastq(std::ostream& output);
		void printRecordFasta();
		void printRecordFasta(std::ostream& output);
		void printRecordQual(int ascii);
		void printRecordQual(std::ostream& output, int ascii);

	private:
		/* data */
		const char* filename;
		unsigned nline;
		char delim;
		std::ostream* output;
		std::istream* input;
		std::istream* qual_input;
		int qual_offset;
		int qual_ascii;
		std::string id;
		std::string idline;
		std::string sequence;
		std::string plusline;
		std::string quality;
		bool validateFastq();
		bool validateFasta();
};

#endif

