/* BioLite - Tools for processing gene sequence data and automating workflows
 * Copyright (c) 2012-2013 Brown University. All rights reserved.
 * 
 * This file is part of BioLite.
 * 
 * BioLite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * BioLite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with BioLite.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include "hashmap.hpp"
#include "seqio.hpp"

#define PROGNAME "exclude"
#include "util.h"

using namespace std;

void print_usage()
{
	cout << "\n"
"usage: "PROGNAME" -x EXCLUDE_FILE [-k] [...] [-i INPUT ...] [-o OUTPUT ...]\n"
"\n"
"Filters all the reads in the input files (FASTA or FASTQ is automatically\n"
"detected) and excludes those with ids found in any of the EXCLUDE_FILEs.\n"
"\n"
"If no inputs are specified, input is read from stdin. If no outputs are\n"
"specified, stdout is used.\n"
"\n"
"If multiple input files are specified, these are treated as paired files.\n"
"So if a sequence in one input is excluded, its pair is also excluded from\n"
"the same position in all other input files.\n"
"\n"
"  -x  each line in EXCLUDE_FILE is truncated by a space or tab and\n"
"      stored as an id to exclude\n"
"  -k  invert exclude file to keep entries\n"
"  -i  specify multiple INPUT files\n"
"  -o  specify multiple OUTPUT files\n"
"\n"
"Example usage:\n"
"exclude -x ids.txt -i 1.fastq -i 2.fastq -o 1.filtered.fastq -o 2.filtered.fastq\n"
"\n";
}

#if DEBUG
void print_exclude_list(const char* filename, IntHash* exclusions)
{
	NOTIFY("exclude list '" << filename  << "':")
	IntHash::iterator i;
	for (i = exclusions->begin(); i != exclusions->end(); ++i)
		cout << i->first << " " << i->second << endl;
}
#endif

void read_exclude_file(
		const char* filename,
		int exclude_id,
		IntHash* exclusions
		)
{
	ifstream exclude_file(filename);
	if (exclude_file.is_open()) {
		while (exclude_file.good()) {
			string line;
			getline(exclude_file, line);
			/* Ignore blank lines. */
			if (line.size() == 0) continue;
			/* The id is the first field of the text input, so truncate on a
			 * space or tab. */
			size_t end = line.find_first_of(" \t");
			if (end != string::npos) line.resize(end);
			/* Insert the pair (sequence_id, exclude_list_id) into the map. */
			exclusions->insert(IntHash::value_type(line, exclude_id));
		}
		exclude_file.close();
	} else {
		ERROR("could not open file '" << filename << "'")
	}
#if DEBUG
	/* Print out hash map. */
	print_exclude_list(filename, exclusions);
#endif
}	

void exclude(
		vector<SeqIO*>& inputs,
		vector<const char*>& exclude_names,
		IntHash* exclusions,
		bool keep
		)
{
	/* Keep track of sequences read and kept. */
	long npairs = 0;
	long nkept = 0;

	/* Keep track of exclusions per exclude file. */
	vector<long> nexc(exclude_names.size(), 0);
	/* And total exclusions. */
	long nexc_total = 0;

	bool good = true;

	/* Advance all of the input files. */
	for (unsigned i=0; i<inputs.size(); i++) {
		/* All inputs must have a record in order to proceed. */
		good &= inputs[i]->nextRecord();
	}

	while (good)
	{
		npairs++;

		IntHash::iterator it;
		bool keep_pair = true;

		for (unsigned i=0; i<inputs.size(); i++) {
			it = exclusions->find(inputs[i]->getID());
			/* The iterator is set to end() if no key is found. */
			if (it != exclusions->end()) {
				/* Lookup the hash map's value for the key, which is the id
				   for the exclude file. */
				int i = it->second;
				nexc[i]++;
				nexc_total++;
				keep_pair = false;
				break;
			}
		}

		if (keep_pair != keep) {
			nkept++;
			for (unsigned i=0; i<inputs.size(); i++)
				inputs[i]->printRecord();
		}

		/* Advance again. */
		for (unsigned i=0; i<inputs.size(); i++)
			good &= inputs[i]->nextRecord();
	}

	DIAGNOSTICS("pairs", npairs, "(sequence pairs found)")
	DIAGNOSTICS("pairs_kept", nkept, "(pairs remaining after filtering)")
	DIAGNOSTICS("total_exclusions", nexc_total, "")
	for (unsigned i=0; i<exclude_names.size(); i++) {
		DIAGNOSTICS(exclude_names[i], nexc[i], "(exclusions from this file)")
	}
}

int main(int argc, char** argv)
{
	vector<const char*> exclude_names;
	vector<const char*> input_names;
	vector<const char*> output_names;
	IntHash* exclusions = new IntHash();
	bool keep = false;

	int c;
	while ((c = getopt(argc, argv, "vhx:i:o:k")) != -1)
		switch (c) {
			case 'x':
				exclude_names.push_back(optarg);
				break;
			case 'i':
				input_names.push_back(optarg);
				break;
			case 'o':
				output_names.push_back(optarg);
				break;
			case 'k':
				keep = true;
				break;
			case 'v':
				PRINT_VERSION
			case 'h':
			default:
				print_usage();
				exit(EXIT_SUCCESS);
		}

	if (exclude_names.size() == 0)
		ARG_ERROR("you must specify at least one exclude file with -x")

	/* If the number of inputs and outputs don't match, and the number of
	   outputs is non-zero, there is a problem! (i.e. how do you map one input
	   stream to multiple output files?) */
	if (input_names.size() != output_names.size() && output_names.size() > 0)
		ARG_ERROR("mismatch between number of input and output files")

	/* Default to stdin for input. */
	if (input_names.size() == 0) input_names.push_back("-");

	/* Read exlude files into the hash map. */
	for (unsigned i=0; i<exclude_names.size(); i++)
		read_exclude_file(exclude_names[i], i, exclusions);

	/* Open the input and output files. */
	vector<SeqIO*> inputs;
	for (unsigned i=0; i<input_names.size(); i++) {
		inputs.push_back(new SeqIO(input_names[i]));
		if (output_names.size() > 0) {
			inputs[i]->setOutput(output_names[i]);
		}
	}

	/* Perform the exclusion. */
	exclude(inputs, exclude_names, exclusions, keep);

	return EXIT_SUCCESS;
}

