#!/bin/bash

set -o errexit

print_usage() {
	echo "
usage: multiblast BLAST THREADS QUERY_LIST OUT [ARGS]

Runs a Blast PROGRAM (e.g. blastx, blastn, blastp) in parallel on a list of
queries (in QUERY_LIST).  Additional arguments to PROGRAM can be appended as
ARGS.

The PROGRAM is called on each query with threading equal to THREADS.
Recommendation: set THREADS to the number of cores divided by the number of
query files.

The individual XML outputs for each query file are concatenated into a single
output file OUT.

Example usage:
multiblast blastn 4 \"query1.fa query2.fa\" all-queries.xml -db nt -e 1e-6
"
}

if [ $# -lt 4 ]; then
	print_usage
	exit 1
fi

PROGRAM=$1
shift
THREADS=$1
shift
QUERY_LIST=$1
shift
OUT=$1
shift

DATE=$(date +%s)

for query in $QUERY_LIST
do
	CMD="$PROGRAM $* -num_threads $THREADS -query $query -out $query.$DATE.xml -outfmt 5"
	echo $CMD
	$CMD &
done
wait

# Merge XML output.
echo "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>
<!DOCTYPE BlastOutput PUBLIC \"-//NCBI//NCBI BlastOutput/EN\" \"http://www.ncbi.nlm.nih.gov/dtd/NCBI_BlastOutput.dtd\">
<BlastOutput>" >$OUT
for query in $QUERY_LIST
do
	# Remove the first 3 lines (XML doctype and doc tag) and last line from
	# each file.
	sed -e '/?xml/d' -e '/DOCTYPE/d' -e '/BlastOutput>/d' $query.$DATE.xml >>$OUT
	rm $query.$DATE.xml
done
# Close doc tag.
echo "</BlastOutput>" >>$OUT

