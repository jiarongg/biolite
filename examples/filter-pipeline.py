from biolite.pipeline import BasePipeline
from biolite.wrappers import FilterIllumina

pipe = BasePipeline('filter', "Example pipeline")

pipe.add_argument('input', short='i',
	help="Input FASTA or FASTQ file to filter.")

pipe.add_argument('quality', short='q', type=int, metavar='MIN',
	default=28, help="Filter out reads that have a mean quality < MIN.")

@pipe.stage
def filter(input, quality):
	'''
	Filter out low-quality and adapter-contaminated reads
	'''
	output = input + '.filtered'
	FilterIllumina([input], [output], quality=quality)
	ingest('output')

if __name__ == "__main__":
	pipe.parse_args()
	pipe.run()

