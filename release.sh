#!/bin/bash

# A script for generating a tarball from the git repository

set -e

# Documentaiton
#
# Requires sphinx and pandoc
#
# sphinx can be installed as follows on OS X:
# sudo easy_install pip
# sudo pip install sphinx
#
# pandoc can be installed from:
# http://johnmacfarlane.net/pandoc/installing.html
#

# Update the install section
pandoc -o doc/install.rst -f markdown -t rst INSTALL.md

# Configure the python package
./configure

export BIOLITE_CONFIG=$PWD/share/biolite.cfg
cd doc
ln -s ../python biolite
export PYTHONPATH=$PWD:$PYTHONPATH

make clean
make html latexpdf
rm -rf ../BioLiteManual
cp -r _build/html ../BioLiteManual
cp _build/latex/BioLite.pdf ../BioLiteManual.pdf

rm biolite
cd ..

# Package
make dist-gzip
make distclean

