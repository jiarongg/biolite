#!/bin/bash

if [ "$1" == "-h" ]
then
	echo "usage: build_3rd_party.sh [PREFIX] [CC] [CXX] [OPT]" >&2
	exit 0
fi

OSNAME=`uname`
OSBIT=`uname -m`
if [ "$OSBIT" != "x86_64" ]
then
	echo "ERROR: you're OS does not appear to be 64-bit" >&2
	exit 1
fi

PREFIX=${1-"/usr/local"}
CC=${2-"gcc"}
CXX=${3-"g++"}
OPT=${4-"-O3"}

BUILD_DIR="build"

set -o nounset
set -o errexit

# Convert relative path to absolute path.
[ "${PREFIX:0:1}" != "/" ] && PREFIX="$PWD/$PREFIX"

echo "Installing to $PREFIX"

BINDIR=$PREFIX/bin
LIBDIR=$PREFIX/lib
OPTDIR=$PREFIX/opt

download() {
	# $1:URL $2:SHA1 $3:FILE
	echo "Downloading $1 ..."
	if [ -f $3 ]
	then
		if [ `shasum $3 | cut -d ' ' -f 1` != "$2" ]
		then
			echo "WARNING: removing corrupt download '$3'" >&2
			rm $3
		fi
	fi
	[ -f $3 ] || wget $1 -O $3
	if [ `shasum $3 | cut -d ' ' -f 1` != "$2" ]
	then
		echo "ERROR: downloaded file is corrupt? (wrong SHA1 sum)" >&2
		exit 1
	fi
}

need_programs() {
	for program in $@
	do
		program=$BINDIR/$program
		[ -h $program ] && program=`readlink $program`
		(command -v $program >/dev/null) || return 0
	done
	return 1
}

need_libs() {
	for lib in $@
	do
		[ -f $LIBDIR/$lib ] || return 0
	done
	return 1
}

need_other() {
	for other in $@
	do
		[ -f $other ] || return 0
	done
	return 1
}

mkdir -p $BUILD_DIR
mkdir -p $BINDIR
mkdir -p $LIBDIR
mkdir -p $OPTDIR

cd $BUILD_DIR

# FastQC
URL="http://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v0.10.1.zip"
FILE=`basename $URL`
SHA1="d1d9c1489c46458614fcedcbbb30e799a0e796a2"
if need_programs fastqc
then
	echo "Installing FastQC..." >&2
	download $URL $SHA1 $FILE
	unzip -qo $FILE
	mv FastQC $OPTDIR/
	chmod a+x $OPTDIR/FastQC/fastqc
	ln -s $OPTDIR/FastQC/fastqc $BINDIR/fastqc
else
	echo "FastQC is already installed in $PREFIX ...skipping." >&2
fi

# MACSE
URL="http://mbb.univ-montp2.fr/MBB/uploads/macse_v0.9b1.jar"
FILE=`basename $URL`
SHA1="36ba126fa7d77b22016b8f0bf72d5b13d1a6c4f4"
if need_other $BINDIR/$FILE
then
	echo "Installing MACSE..." >&2
	download $URL $SHA1 $FILE
	cp $FILE $BINDIR/
else
	echo "MACSE is already installed in $PREFIX ...skipping." >&2
fi

# mcl
URL="http://micans.org/mcl/src/mcl-12-135.tar.gz"
FILE=`basename $URL`
SHA1="27e7bc08fe5f0d3361bbc98d343c9d045712e406"
if need_programs mcl
then
	echo "Installing MCL..." >&2
	download $URL $SHA1 $FILE
	tar xzf $FILE
	cd ${FILE%.tar.gz}
	./configure --prefix="$PREFIX" CC="$CC" CFLAGS="$OPT"
	make install
	cd ..
else
	echo "MCL is already installed in $PREFIX ...skipping." >&2
fi

# Gblocks
FILE=""
SHA1=""
if [ "$OSNAME" == "Darwin" ]
then
	FILE="Gblocks_OSX_0.91b.tar.Z"
	SHA1="2e453593516f44a71201eb6fcb8b1fbf2e868068"
elif [ "$OSNAME" == "Linux" ]
then
	FILE="Gblocks_Linux64_0.91b.tar.Z"
	SHA1="149d8ae44346eab8e39b71309aa8961f37ffde76"
else
	echo "WARNING: cannot install Gblocks for OS '$OSNAME'" >&2
fi
URL="http://molevol.cmima.csic.es/castresana/Gblocks/$FILE"
if [ -n "$FILE" ] && need_programs Gblocks
then
	echo "Installing Gblocks..." >&2
	download $URL $SHA1 $FILE
	tar xzf $FILE
	cp Gblocks_0.91b/Gblocks $BINDIR/
else
	echo "Gblocks is already installed in $PREFIX ...skipping." >&2
fi

# RAxML
URL="http://sco.h-its.org/exelixis/countSource728.php"
FILE="RAxML-7.2.8-ALPHA.tar.bz2"
SHA1="06088d8db5e66193604b6837cb1aec226895aa58"
if need_programs raxmlHPC-PTHREADS-SSE3
then
	echo "Intalling RAxML..." >&2
	download $URL $SHA1 $FILE
	tar xjf $FILE
	cd ${FILE%.tar.bz2}
	make -f Makefile.SSE3.PTHREADS.gcc CC="$CC"
	cp raxmlHPC-PTHREADS-SSE3 $BINDIR/
	cd ..
else
	echo "RAxML is already installed in $PREFIX ...skipping." >&2
fi

# Velvet
URL="http://www.ebi.ac.uk/~zerbino/velvet/velvet_1.2.08.tgz"
FILE=`basename $URL`
SHA1="81432982c6a0a7fe8e5dd46fd5e88193dbd832aa"
#VELVET=${FILE%.tgz}
VELVET="velvet"
if need_programs velveth velvetg oases
then
	echo "Installing Velvet..." >&2
	download $URL $SHA1 $FILE
	tar xzf $FILE
	cd $VELVET
	make CC="$CC" OPT="$OPT" OPENMP=1 MAXKMERLENGTH=63 LONGSEQUENCES=1 velveth velvetg
	cp velveth velvetg $BINDIR/
	cd ..
else
	echo "Velvet is already installed in $PREFIX ...skipping." >&2
fi

# Oases
URL="http://www.ebi.ac.uk/~zerbino/oases/oases_0.2.08.tgz"
FILE=`basename $URL`
SHA1="a06a9888ff97e505af7eeb0f93bccfb0f00169b5"
OASES="oases_0.2.8"
if need_programs oases
then
	echo "Installing Oases..." >&2
	download $URL $SHA1 $FILE
	tar xzf $FILE
	cd $OASES
	make CC="$CC" OPT="$OPT" OPENMP=1 MAXKMERLENGTH=63 LONGSEQUENCES=1 VELVET_DIR="../$VELVET" oases
	cp oases $BINDIR/
	cd ..
else
	echo "Oases is already installed in $PREFIX ...skipping." >&2
fi

# Trinity
URL="http://downloads.sourceforge.net/trinityrnaseq/trinityrnaseq_r2013-02-25.tgz"
FILE=`basename $URL`
SHA1="5fdb2682f861828750add2152154ec515e6e78c8"
TRINITY=${FILE%.tgz}
if need_programs Trinity.pl || need_other $BINDIR/Butterfly.jar
then
	echo "Installing Trinity..." >&2
	download $URL $SHA1 $FILE
        tar xzf $FILE
	cd $TRINITY
	make CXX="$CXX" CXXFLAGS="$OPT"
	cd ..
	mv $TRINITY $OPTDIR/
	ln -s $OPTDIR/$TRINITY/Trinity.pl $BINDIR/Trinity.pl
	ln -s $OPTDIR/$TRINITY/Butterfly/Butterfly.jar $BINDIR/Butterfly.jar
else
	echo "Trinity is already installed in $PREFIX ...skipping." >&2
fi

# Bowtie2
URL="http://downloads.sourceforge.net/project/bowtie-bio/bowtie2/2.0.6/bowtie2-2.0.6-source.zip"
FILE=`basename $URL`
SHA1="86068dd90312fca2f8068412cbf469c056366220"
if need_programs bowtie2 bowtie2-align bowtie2-build bowtie2-inspect
then
	echo "Installing Bowtie2..." >&2
	download $URL $SHA1 $FILE
	unzip -qo $FILE
	cd ${FILE%-source.zip}
	make CC="$CC" CPP="$CXX" CXX="$CXX" BOWTIE_PTHREADS=1
	cp bowtie2 bowtie2-build bowtie2-align bowtie2-inspect $BINDIR/
	cd ..
else
	echo "Bowtie2 is already installed in $PREFIX ...skipping." >&2
fi

# Bamtools
URL="https://github.com/pezmaster31/bamtools/archive/v2.2.3.tar.gz"
FILE="bamtools-2.2.3.tar.gz"
SHA1="130c20f13948e007516d2d0ec068bcb9685fc8d4"
if need_libs bamtools/libbamtools.a
then
	echo "Installing Bamtools..." >&2
	download $URL $SHA1 $FILE
	tar xzf $FILE
	cd ${FILE%.tar.gz}
	mkdir build
	cd build
	cmake -DCMAKE_INSTALL_PREFIX:PATH=$PREFIX ..
	make install
	cd ../..
else
	echo "Bamtools is already installed in $PREFIX ...skipping." >&2
fi

# SGA
URL="https://github.com/jts/sga/tarball/v0.9.42"
FILE="sga-0.9.42.tgz"
SHA1="dd2c778cbfc763f2877d07a6b67d35a59c71ce61"
SGA="jts-sga-df7ceb0"
if need_programs sga
then
	echo "Installing SGA..." >&2
	download $URL $SHA1 $FILE
	tar xzf $FILE
	cd $SGA/src
	sed -i -e '/Werror/ d' configure.ac
	./autogen.sh
	./configure --with-bamtools="$PREFIX" --prefix="$PREFIX" CC="$CC" CXX="$CXX"
	make install
	cd ../..
else
	echo "SGA is already installed in $PREFIX ...skipping." >&2
fi

# CGAL
URL="http://bio.math.berkeley.edu/cgal/cgal-0.9.6-beta.tar"
FILE=`basename $URL`
SHA1="154f70d4673b7b0621cb93e16eb604710ccb4ed8"
if need_programs align bowtie2convert cgal
then
	echo "Installing CGAL..." >&2
	download $URL $SHA1 $FILE
	tar xf $FILE
	cd ${FILE%.tar}
	make CC="$CC" CPP="$CXX" CXX="$CXX"
	cp align bfastconvert bowtie2convert cgal $BINDIR/
	cd ..
else
	echo "CGAL is already installed in $PREFIX ...skipping." >&2
fi

# SRA Toolkit
FILE=""
SHA1=""
if [ "$OSNAME" == "Linux" ]
then
	FILE="sratoolkit.2.3.2-5-ubuntu64.tar.gz"
	SHA1="0f8df4b477408104ac4a17a0da2406db69239763"
else
	echo "WARNING: cannot install SRA Toolkit for OS '$OSNAME'" >&2
fi
URL="http://ftp-trace.ncbi.nlm.nih.gov/sra/sdk/2.3.2-5/$FILE"
if [ -n "$FILE" ] && need_programs fastq-dump
then
	echo "Installing SRA Toolkit..." >&2
	download $URL $SHA1 $FILE
	tar xzf $FILE
	cp -r sratoolkit.2.3.2-5-ubuntu64/bin/* $BINDIR/
else
	echo "SRA Toolkit is already installed in $PREFIX ...skipping." >&2
fi

# GNU parallel
URL="http://ftp.gnu.org/gnu/parallel/parallel-20130622.tar.bz2"
FILE=`basename $URL`
SHA1="032c4b57dbf21156a6706481e3676ac3cd77efdf"
if need_programs parallel
then
	echo "Installing GNU parallel..." >&2
	download $URL $SHA1 $FILE
	tar xjf $FILE
	cd ${FILE%.tar.bz2}
	./configure
	make install
	cd ..
else
	echo "GNU parallel is already installed in $PREFIX ...skipping." >&2
fi

