#!/bin/sh

BIOLITE="biolite-0.3.3"
FILE="$BIOLITE.tar.gz"
URL="https://bitbucket.org/caseywdunn/biolite/downloads/$FILE"
LOG=`mktemp /tmp/install-biolite-XXXX`

set -e

if [ "$USER" != "root" ]; then
	echo "You must run this install script as root (e.g. with 'sudo')."
	exit 1
fi


echo "+ Install log is located in: $LOG"

echo "+ Installing dependencies with apt-get"
apt-get -qq install \
	automake autoconf libtool cmake \
	python-docutils python-matplotlib python-biopython python-networkx \
 	python-pip python-lxml curl zip default-jre \
	g++ zlib1g-dev libncurses5-dev libsparsehash-dev pkg-config \
	bowtie ncbi-blast+ samtools

echo "+ Installing dendropy with pip"
pip install dendropy >>$LOG

echo "+ Installing wget with pip"
pip install wget >>$LOG

if [ -f ./build_3rd_party.sh ];
then
	echo "+ Found local repository for BioLite: running autogen..."
	./autogen.sh >>$LOG
else
	echo "+ Retrieving remote tar ball for BioLite..."
	echo "+ Downloading $URL to /usr/local/src"
	mkdir -p /usr/local/src
	cd /usr/local/src
	wget $URL >>$LOG

	echo "+ Extracting $FILE"
	tar xf $FILE
	cd $BIOLITE
fi

echo "+ Building 3rd party software in /usr/local"

./build_3rd_party.sh >>$LOG

echo "+ Installing BioLite in /usr/local"
./configure >>$LOG
make install >>$LOG

echo "+ Finished installation.

  BioLite commands should now be in your PATH (in /usr/local/bin).
"

